import React from 'react';
import { Text } from 'evergreen-ui'
import errorCodeMap from '../utils/errorCodeMap'

function ErrorCode({ match }) {
  const { params: { code } = {} } = match
  return <Text size={500} paddingY={8}>{errorCodeMap(code)}</Text>
}

export default ErrorCode