import React, { useState, useEffect } from 'react';
import { Table, Pane, Button } from 'evergreen-ui';
import Main from '../layout/Main';

function I18n() {
  const [v1, setV1] = useState(1);
  const [v2, setV2] = useState(1);
  const [v3, setV3] = useState(1);

  useEffect(() => {
    setV2(v1 + 1);
  }, [v1]);

  useEffect(() => {
    setV3(v2 + 1);
  }, [v2]);

  return (
    <Main title="TEST2">
      <Pane paddingY={4} />
      <Table>
        <Table.Head>
          <Table.TextHeaderCell>label</Table.TextHeaderCell>
          <Table.TextHeaderCell>value</Table.TextHeaderCell>
          <Table.TextHeaderCell>feature</Table.TextHeaderCell>
        </Table.Head>
        <Table.Body>
          <Table.Row>
            <Table.TextCell>v1</Table.TextCell>
            <Table.TextCell>{v1}</Table.TextCell>
            <Table.TextCell>
              <Button
                height={24}
                onClick={() => {
                  setV1(v1 + 1);
                }}
              >
                ADD
              </Button>
            </Table.TextCell>
          </Table.Row>
          <Table.Row>
            <Table.TextCell>v2</Table.TextCell>
            <Table.TextCell>{v2}</Table.TextCell>
            <Table.TextCell>
              <Button height={24}>ADD</Button>
            </Table.TextCell>
          </Table.Row>
          <Table.Row>
            <Table.TextCell>v3</Table.TextCell>
            <Table.TextCell>{v3}</Table.TextCell>
            <Table.TextCell>
              <Button
                height={24}
                onClick={() => {
                  setV3(v3 + 1);
                }}
              >
                ADD
              </Button>
            </Table.TextCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Main>
  );
}

export default I18n;
