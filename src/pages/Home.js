import React from 'react';
import Main from '../layout/Main';
import HomeCover from '../components/HomeCover';

function Home() {
  return (
    <Main title="Home">
      <div className="page page-home">
        <HomeCover />
      </div>
    </Main>
  );
}

export default Home;
