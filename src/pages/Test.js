import React from 'react';
import { Pane, Button, Text } from 'evergreen-ui';
import { useSelector } from 'react-redux';
import { persistor } from '../store'
import Main from '../layout/Main';

function Test(props) {
  const { name: username } = useSelector(state => state.user);
  return (
    <Main title="Test">
      <Pane display="flex" alignItems="center">
        <Pane paddingX={20} paddingY={8}>
          <Text size={400}>{username}</Text>
          <Button
            onClick={async () => {
              console.log('persistor')
            }}
          >
            TEST
          </Button>
        </Pane>
      </Pane>
    </Main>
  );
}

export default Test;
