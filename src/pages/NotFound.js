import React from 'react';
import { Pane, Heading } from 'evergreen-ui'
import Main from '../layout/Main'

function NotFound() {
  return <Main title="Not Found">
    <div className="page page-not-found">
      <Pane display="flex" alignItems="center"
        justifyContent="center">
        <Heading size={900} marginTop={12}>404</Heading>
      </Pane>
    </div>
  </Main>
}

export default NotFound;