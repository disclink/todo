import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import jsCookie from 'js-cookie';
import { Pane, Avatar, Text, Button } from 'evergreen-ui';
import Main from '../layout/Main';
import { userRemove } from '../actions/user';
import { ReactComponent as GithubIcon } from '../assets/img/github.svg';
import { ReactComponent as UserIcon } from '../assets/img/user.svg';
import {CLIENT_ID} from '../utils/constant'

function User() {
  const  { name: username, uid, avatar_url } = useSelector(state => state.user);
  const dispatch = useDispatch();

  function doLogout() {
    jsCookie.remove('connect.sid');
    dispatch(userRemove());
  }

  return (
    <Main title="User">
      <Pane
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        paddingY={24}
      >
        {!!username && !!uid
          ? [
              <Pane key="b1" flex={1}>
                {!!avatar_url ? (
                  <Avatar
                    hashValue="id_124"
                    src={avatar_url}
                    name={username}
                    size={108}
                    marginTop={24}
                  />
                ) : (
                  <UserIcon
                    style={{
                      width: '108px',
                      marginTop: '24px',
                      fill: '#F5F6F7',
                      stroke: '#425A70',
                      overflow: 'visible',
                      strokeWidth: 2
                    }}
                  />
                )}
              </Pane>,
              <Pane key="b2" flex={1} paddingY={4}>
                <Text size={600}>{username}</Text>
              </Pane>,
              <Pane key="b3" flex={1} paddingY={4}>
                <Button marginY={24} onClick={doLogout}>
                  logout
                </Button>
              </Pane>
            ]
          : [
              <Pane key="a1" flex={1}>
                <UserIcon
                  style={{
                    width: '108px',
                    marginTop: '24px',
                    fill: '#F5F6F7',
                    stroke: '#425A70',
                    overflow: 'visible',
                    strokeWidth: 2
                  }}
                />
              </Pane>,
               <Pane key="a2" flex={1} paddingY={4}>
               <Text size={600}>&nbsp;</Text>
             </Pane>,
              <Pane key="a3" flex={1} paddingY={4}>
                <Button
                  marginY={24}
                  is="a"
                  href={`https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user&allow_signup=true`}
                >
                  <GithubIcon
                    style={{
                      width: '16px',
                      paddingRight: '8px',
                      fill: '#425A70'
                    }}
                  />
                  login
                </Button>
              </Pane>
            ]}
      </Pane>
    </Main>
  );
}

export default User;
