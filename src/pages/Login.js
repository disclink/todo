import React from 'react';
import { Pane, Button } from 'evergreen-ui'
import Main from '../layout/Main'
import {CLIENT_ID} from '../utils/constant'

function Login() {
  return <Main title="Login">
    <Pane display="flex" justifyContent="center" alignItems="center">
      <Button marginY={24} is="a"  appearance="primary" href={`https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user&allow_signup=true`}>LOGIN</Button>
    </Pane>
  </Main>
}

export default Login