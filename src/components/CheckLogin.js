import React from 'react';
import { connect } from 'react-redux';
import jsCookie from 'js-cookie';
import axios from 'axios';
import { userAdd, userRemove } from '../actions/user';

class CheckLogin extends React.Component {
  async fetchUserInfo() {
    // is it logged in
    const sid = jsCookie.get('connect.sid');
    if (!sid) {
      this.props.userRemove();
      return;
    }
    // userinfo exists in the store
    let { uid = '', name = '', avatar_url = '', sid: stateSid = '' } = this.props.user;
    if (!!stateSid && stateSid !== sid) {
      jsCookie.remove('connect.sid');
      this.props.userRemove();
      return;
    } else if (!!uid && !!name && !!avatar_url) {
      return;
    }
    let result = {};
    try {
      result = await axios.get('/api/user').then(r => r.data.playload);
    } catch (error) {
      
    }
    if (!result || !result.uid || !result.name) {
      jsCookie.remove('connect.sid');
      this.props.userRemove();
      return;
    }
    this.props.userAdd({
      uid: result.uid,
      name: result.name,
      avatar_url: result.avatar_url,
      sid
    });
    return;
  }

  componentDidMount() {
    this.fetchUserInfo();
  }

  render() {
    return null;
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  {
    userAdd,
    userRemove
  }
)(CheckLogin);
