import { Component } from 'react';

class DocumentTitle extends Component {
  static defaultTitle = 'react-app'

  componentDidMount() {
    document.title = this.props.title || this.defaultTitle
  }

  componentWillUnmount() {
    document.title = this.defaultTitle
  }


  render() {
    return (null)
  }

}

export default DocumentTitle