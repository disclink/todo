import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Pane, Button, TextInput, IconButton } from 'evergreen-ui';
import { todoAdd, todosFilter } from '../actions/todos';

function TodoInput({ switchInputType }) {
  const [text, setText] = useState();
  const dispatch = useDispatch();

  function sumbit() {
    const t = text.toString().trim();
    if (!t) return;
    dispatch(todoAdd(t));
    dispatch(todosFilter(''));
    setText('');
    return;
  }

  return (
    <Pane
      paddingX={24}
      paddingY={12}
      display="flex"
      borderBottom="muted"
      background="#fff"
      alignItems="center"
      justifyContent="center"
    >
      <Pane display="flex" width={360} flexDirection="row">
        <TextInput
          flex={1}
          placeholder="write something"
          value={text || ''}
          onChange={e => setText(e.target.value)}
          onKeyPress={e => {
            if (e.key && e.key === 'Enter') {
              e.preventDefault();
              sumbit();
            }
          }}
        />
        <Pane width={24} />
        <Button iconBefore="menu-open" onClick={sumbit}>
          add
        </Button>
        <Pane width={12} />
        <IconButton icon="comparison" onClick={switchInputType} />
      </Pane>
    </Pane>
  );
}

export default TodoInput;
