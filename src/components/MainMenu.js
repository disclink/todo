import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import jsCookie from 'js-cookie';
import { IconButton, Popover, Menu, Position, Avatar } from 'evergreen-ui';
import { userRemove } from '../actions/user';
import { CLIENT_ID } from '../utils/constant';

function MainMenu({ history, options }) {
  const { name: username, uid, avatar_url } = useSelector(state => state.user);
  const dispatch = useDispatch();

  function doLogout() {
    jsCookie.remove('connect.sid');
    dispatch(userRemove());
  }

  return (
    <Popover
      position={Position.BOTTOM_RIGHT}
      content={
        <Menu>
          {!!username &&
            !!uid && [
              <Menu.Group key="username">
                <Menu.Item onSelect={() => {history.push('/user')}}>
                  {!!avatar_url && (
                    <Avatar
                      hashValue="id_124"
                      src={avatar_url}
                      name={username}
                      size={24}
                      marginRight={8}
                      style={{ verticalAlign: 'middle' }}
                    />
                  )}
                  {username}
                </Menu.Item>
              </Menu.Group>,
              <Menu.Divider key="divider" />
            ]}
          <Menu.Group>
            {options.map(
              ({ label, path }, index) =>
                !!label &&
                !!path && (
                  <Menu.Item key={index} onSelect={() => history.push(path)}>
                    {label}
                  </Menu.Item>
                )
            )}
          </Menu.Group>
          <Menu.Divider />
          <Menu.Group>
            {!!username && !!uid ? (
              <Menu.Item onSelect={doLogout}>Logout</Menu.Item>
            ) : (
              <Menu.Item
                onSelect={() => {
                  window.location = `https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user&allow_signup=true`;
                }}
              >
                Login
              </Menu.Item>
            )}
          </Menu.Group>
        </Menu>
      }
    >
      <IconButton appearance="minimal" icon="menu" />
    </Popover>
  );
}

export default withRouter(MainMenu);
