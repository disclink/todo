import React from 'react';
import { useSelector } from 'react-redux';
import { Pane } from 'evergreen-ui';
import TodoItem from './TodoItem';
import { todosFilterStatus } from '../actions/todos';

function TodoList() {
  const { COMPLETED, NOT_COMPLETED } = todosFilterStatus;
  const todos = useSelector(state => state.todos);
  const todosVisibilityFilter = useSelector(
    state => state.todosVisibilityFilter
  );

  return (
    <Pane>
      <Pane background="#fff">
        {todos
          .filter(item => {
            if (todosVisibilityFilter === COMPLETED) {
              return item.completed;
            } else if (todosVisibilityFilter === NOT_COMPLETED) {
              return !item.completed;
            } else {
              return true;
            }
          })
          .reverse()
          .map((item, index) => (
            <TodoItem key={item.id} {...item} />
          ))}
      </Pane>
    </Pane>
  );
}

export default TodoList;
