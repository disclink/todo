import React from 'react';
import { Pane, Text } from 'evergreen-ui';
import appInfo from '../../package.json';

function Footer() {
  const tips = `version: ${appInfo.version || ''}`;
  return (
    <Pane position="sticky" background="#fff" bottom={0}>
      <Pane paddingY={8}>
        <Text>{tips}</Text>
      </Pane>
    </Pane>
  );
}

export default Footer;
