const Dict = {
  '10401': 'Unable to get github access token',
  '10402': 'Access token invalid',
  '10403': 'Unable to get user id or user name'
};

export default function errorCodeMap(code) {
  return Dict[code] || 'Unknown exception';
}
