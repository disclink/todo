export function setDocumentTitle(title) {
  document.title = title || 'react-app'
}