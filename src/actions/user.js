export const USER_ADD = 'USER_ADD';
export const USER_REMOVE = 'USER_REMOVE';

export function userAdd(playload) {
  return {
    type: USER_ADD,
    playload
  };
}

export function userRemove(id) {
  return {
    type: USER_REMOVE,
    id
  };
}
