export const TODO_ADD = 'TODO_ADD';
export const TODO_BATCH_ADD = 'TODO_BATCH_ADD';
export const TODO_REMOVE = 'TODO_REMOVE';
export const TODO_TOGGLE = 'TODO_TOGGLE'
export const TODOS_FILTER = 'TODOS_FILTER'

export const todosFilterStatus = {
  COMPLETED: 'COMPLETED',
  NOT_COMPLETED: 'NOT_COMPLETED'
}

export function todoAdd(text) {
  return {
    type: TODO_ADD,
    text
  }
}

export function todoBatchAdd(texts) {
  return {
    type: TODO_BATCH_ADD,
    texts
  }
}

export function todoRemove(id) {
  return {
    type: TODO_REMOVE,
    id
  }
}

export function todoToggle(id) {
  return {
    type: TODO_TOGGLE,
    id
  }
}

export function todosFilter(filter) {
  return {
    type: TODOS_FILTER,
    filter
  }
}