import {
  TODO_ADD,
  TODO_BATCH_ADD,
  TODO_REMOVE,
  TODO_TOGGLE,
  TODOS_FILTER
} from '../actions/todos';
import uuidv1 from 'uuid/v1';

export function todos(state = [], action) {
  switch (action.type) {
    case TODO_ADD:
      return [
        {
          id: uuidv1(),
          text: action.text,
          date: new Date() - 0,
          completed: false
        },
        ...state
      ];
    case TODO_BATCH_ADD:
      return [
        ...action.texts.map(text => ({
          id: uuidv1(),
          date: new Date() - 0,
          text: text,
          completed: false
        })),
        ...state
      ];
    case TODO_REMOVE:
      return state.filter(item => item.id !== action.id);
    case TODO_TOGGLE:
      return state.map(item =>
        item.id === action.id
          ? Object.assign({}, item, { completed: !item.completed })
          : item
      );
    default:
      return state;
  }
}

export function todosVisibilityFilter(state = '', action) {
  switch (action.type) {
    case TODOS_FILTER:
      return action.filter;
    default:
      return state;
  }
}
