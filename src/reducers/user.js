import { USER_ADD, USER_REMOVE } from '../actions/user';

export function user(
  state = { uid: '', name: '', avatar_url: '', sid: '' },
  action
) {
  switch (action.type) {
    case USER_ADD:
      const {
        uid = '',
        name = '',
        avatar_url = '',
        sid = ''
      } = action.playload;
      return Object.assign({}, { uid, name, avatar_url, sid });
    case USER_REMOVE:
      return Object.assign({}, { uid: '', name: '', avatar_url: '', sid: '' });
    default:
      return state;
  }
}
