import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import {todos, todosVisibilityFilter} from './todos'
import {user} from './user'

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    todos,
    todosVisibilityFilter,
    user
  });

export default rootReducer;
