import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom'
import { PersistGate } from 'redux-persist/integration/react'
import LoadSpinner from './components/LoadSpinner'
import store, { history, persistor } from './store';
import routes from './routes'
import CheckLogin from './components/CheckLogin'

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <Suspense fallback={<LoadSpinner />}>
          <CheckLogin />
          <Switch>
            {routes.map((route, index) => <Route key={index} {...route} />)}
          </Switch>
        </Suspense>
      </ConnectedRouter>
      </PersistGate>
    </Provider>
  )
}

export default App;
