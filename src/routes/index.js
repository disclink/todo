import React, { lazy } from 'react'
import { Redirect } from "react-router-dom";

const homePage = [
  {
    path: '/',
    exact: true,
    component: lazy(() => import('../pages/Home'))
  },
  {
    path: '/home',
    render: (props) => <Redirect to={{ pathname: '/', state: { from: props.location } }} />
  }
]

const errorPage = [
  {
    component: lazy(() => import('../pages/NotFound'))
  }
]

const routes = [
  {
    path: '/todo',
    component: lazy(() => import('../pages/Todos'))
  },
  {
    path: '/login',
    component: lazy(() => import('../pages/Login'))
  },
  {
    path: '/user',
    component: lazy(() => import('../pages/User'))
  },
  {
    path: '/test',
    component: lazy(() => import('../pages/Test'))
  },
  {
    path: '/error',
    component: lazy(() => import('../pages/Error'))
  }
]

export default [
  ...homePage,
  ...routes,
  ...errorPage
]