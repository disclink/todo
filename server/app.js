const express = require('express');
const path = require('path');
const axios = require('axios');
const session = require('express-session');
const proxy = require('express-http-proxy');
const dotenv = require('dotenv');

const { parsed: envConfig = {} } = dotenv.config();
const {
  OAUTH_CLIENT_ID,
  OAUTH_CLIENT_SECRET,
  SERRVER_SESSION_SECRET,
  SERVER_PORT
} = envConfig;

function jsonFormat(playload = {}, code = 200, msg = '') {
  return Object.assign(
    {},
    {
      code,
      playload,
      msg
    }
  );
}

const app = express();
axios.defaults.headers.post['Content-Type'] = 'application/json';

app.use(
  session({
    secret: SERRVER_SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { httpOnly: false }
  })
);

/* api */
app.get('/api/oauth', async (req, res) => {
  const { code = '' } = req.query;
  console.log(`oauth code: ${code}`);
  console.log(`OAUTH_CLIENT_ID: ${OAUTH_CLIENT_ID}`);
  console.log(`OAUTH_CLIENT_SECRET: ${OAUTH_CLIENT_SECRET}`);
  let accessToken = {};
  let userData = {};
  if (!code) {
    res.redirect('/login');
  } else {
    try {
      console.log('start get accessToken');
      accessToken = await axios({
        method: 'post',
        url: 'https://github.com/login/oauth/access_token',
        headers: {
          Accept: 'application/json'
        },
        data: {
          client_id: OAUTH_CLIENT_ID,
          client_secret: OAUTH_CLIENT_SECRET,
          code
        }
      }).then(r => r.data);
    } catch (error) {}
    const { access_token } = accessToken || {};
    console.log(`access_token: ${access_token}`);
    if (!access_token) {
      res.redirect('/error/10401');
      return;
    }
    try {
      userData = await axios({
        method: 'get',
        url: 'https://api.github.com/user',
        headers: {
          Authorization: `token ${access_token}`
        }
      }).then(r => r.data);
    } catch (error) {
      res.redirect('/error/10402');
    }
    const { id, name, avatar_url } = userData;
    console.log(`id: ${id}`);
    console.log(`name: ${name}`);
    console.log(`avatar_url: ${avatar_url}`);
    if (!!id && !!name) {
      req.session.auth = {
        uid: id,
        name,
        avatar_url,
        github_access_token: access_token
      };
      res.redirect('/user');
    } else {
      res.redirect('/error/10403');
    }
    return;
  }
});

app.get('/api/user', (req, res) => {
  const { uid, name, avatar_url = '' } =
    (req.session && req.session.auth) || {};
  if (!!uid && !!name) {
    res.json(jsonFormat({ uid, name, avatar_url }));
  } else {
    res.json(jsonFormat({}, 408, 'Need to log in again'));
  }
});

app.get('/api/fake/todos', async (req, res) => {
  const { size } = req.query;
  const s = Number(size);
  let count = s > 0 ? s : 1;
  let todos = [];
  console.time('FETCH todo');
  try {
    todos = await axios.get(
      `https://www.loremapi.org/api?paragraphs=${count}-${count}&words=3-20&start-with-lorem-ipsum=false&format=json`
    ).then(res => res.data);
  } catch (error) {
    res.json(jsonFormat({}, 400, 'Third-party service exception'));
  }
  console.timeEnd('FETCH todo');
  if (!Array.isArray(todos) || todos.length <= 0) {
    return res.json(jsonFormat({}, 400, 'No data'));
  }
  todos.forEach((t, i) => {
    console.log(`${i + 1} ${t}`);
  });
  const sequence = todos.join('#');
  return res.json(jsonFormat({ sequence }));
});

app.use('/api/gists/', async function(req, res) {
  const { auth = {} } = req.session || {};
  const { github_access_token } = auth;
  if (!github_access_token) {
    return res.json(jsonFormat({}, 401, 'Permission denied'));
  }
  const { method, params, originalUrl } = req;
  const url = 'https://api.github.com' + originalUrl.replace('/api', '');
  console.log(url);
  let result = null;
  try {
    result = await axios({
      method,
      url,
      headers: {
        Authorization: `token ${github_access_token}`
      },
      data: params
    }).then(r => {
      console.log(r.data);
      return r.data;
    });
  } catch (error) {}
  if (!!result) {
    const { playload = {} } = result;
    const { result: data = {} } = playload;
    res.json(jsonFormat({ data }));
  } else {
    res.json(jsonFormat({}, 400, 'No data'));
  }
});

app.use('/api/*', (req, res) => {
  res.status(404).send('not found');
});
/* api end */

/* static */
app.use(
  express.static(path.resolve(__dirname, '../build'), {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html'],
    index: false,
    maxAge: '1d',
    redirect: false
  })
);

app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, '../build', 'index.html'));
});
/* static end */

app.listen(SERVER_PORT, () => {
  console.log(`service start ${SERVER_PORT}`);
});
